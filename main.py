import cv2
from matplotlib import pyplot as plt
import numpy as np
from PIL import Image
import matplotlib.pyplot as plt
import random
path = '/Users/Vika/Desktop/img_binarization/1.jpeg'
def get_resolution(path):
    with Image.open(path) as img:
        width, height = img.size
        return width, height

def noise(per,path):
    w, h = get_resolution(path)
    image = cv2.imread(path)
    noise=((w*h)/100)*per
    for i in range(int(noise)):
        i=random.randint(0, int(h-1))
        j=random.randint(0, int(w-1))
        bw=random.randint(1,2)
        if bw==1:
            n=0
        else:
            n=255
        image[int(i)][int(j)]=int(n)
    cv2.imshow('with noise', image)
    cv2.waitKey(0)


# читаємо зображення з диску
image = cv2.imread(path)
image_bw = cv2.imread(path,0)

resized_img = cv2.resize(image,(500,300))


b = image.copy()
# зелений та червоний канал на 0
b[:, :, 1] = 0
b[:, :, 2] = 0


g = image.copy()
# синій і червоний канал на 0
g[:, :, 0] = 0
g[:, :, 2] = 0

r = image.copy()
# синій і зелений канал на 0
r[:, :, 0] = 0
r[:, :, 1] = 0


# RGB - синій
cv2.imshow ('B-RGB', b)

# RGB - зелений
cv2.imshow('G-RGB', g)

# RGB - черовний 
cv2.imshow('R-RGB', r)

# гістограма чорнобілої фотографії
plt.hist(image_bw.ravel(),256)

# показ  оригінального фото
cv2.imshow('original_image', image)
# показ  чорнобілого фото фото
cv2.imshow('BW_image', image_bw)
# показ  зменшеного фото фото
cv2.imshow('small-RGB', resized_img)

# еквалізація гістограми чорнобілого зображення
clahe = cv2.createCLAHE(clipLimit=2.0, tileGridSize=(8,8))
cl1 = clahe.apply(image_bw)
# вивід гістограми еквалізованого зображення
plt.hist(cl1.ravel(),256)

cv2.imshow('ecvalize_bw',cl1)

noise(5, path)
plt.show()

cv2.waitKey()

